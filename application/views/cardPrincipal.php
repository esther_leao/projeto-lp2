<div class="row mb-4">
    <div class="col-md-5 mx-auto mt-4">
        <div class="card card-cascade wider">
          <div class="view view-cascade overlay">
              <img  class="card-img-top" src="<?= base_url('img/esther.jpg')?>" alt="Card image cap">
          </div>
        	<div class="card-body card-body-cascade text-center">
            <h4 class="card-title"><strong>Esther Leão</strong></h4>
            <!-- Subtitle -->
            <h5 class="blue-text pb-2"><strong>GU3002489</strong></h5>
            <!-- Text -->
            <p class="card-text">
                3 Semestre - Turma A<br>
                Professor: Reginaldo Prado
            </p>
          </div>
       </div>
    </div>
  	<div class="col-md-5 mx-auto mt-4">
        <div class="card card-cascade wider">
          <div class="view view-cascade overlay" >
              <img class="card-img-top" src="<?= base_url('img/teste2.png')?>" alt="Card image cap">
          </div>
        	<div class="card-body card-body-cascade text-center">
            <h4 class="card-title"><strong>Consumo de API's</strong></h4>
            <h5 class="blue-text pb-2"><strong>Gisgraphy Web Services</strong></h5>
            <!-- Text -->
            <p class="card-text">
            <a href="<?= base_url('Usuario/Gisgraphy_definicao')?>" class="black-text d-flex justify-content-end"><h6>Leia mais <i class="fas fa-angle-double-right"></i></h6></a>
            </p>
          </div>
       </div>
</div>