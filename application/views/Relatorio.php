
<div class="card card-cascade wider reverse mt-3 ml-5 mr-5">
    <img class="" src="../img/gisgraphy.png">

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center">
    <!-- Title -->
    <h4 class="card-title"><strong>Relatório - Gisgraphy Webservices</strong></h4>
    <!-- Text -->
    <p class="card-text">O Gisgraphy permite que o usuário procure por endereços das mais diversas formas. Por exemplo, o usuário pode optar por inserir a latitude e longitude do lugar e receber o nome da rua. Ou inserir o nome de uma cidade e receber diversas informações sobre ela .
    </p>
    <p class="card-text">Podemos dizer que é possível perceber que os desenvolvedores tem uma grande preocupação com a qualidade do serviço, por isso, é usado um conjunto complexo de regras e algoritmos para limpar e garantir que não hajam dados duplicados, além de “misturar” outras fontes de conjuntos de dados para combinar seus pontos fortes. O que resulta em um banco de dados exclusivo e abrangente.
    </p>
    <p class="card-text">O Gisgraphy é uma API flexível , que possui uma boa e extensa documentação e pode ser usada em diversos casos diferentes, no seu site é possível encontrar vários estudos de caso que ajudam o usuário a ver se a api atende suas necessidades. Além disso, o usuário pode escolher em qual linguagem usar a api, o que facilita seu trabalho.
    </p>
  </div>

</div>
