<!-- News jumbotron -->
<div class="jumbotron text-center hoverable p-4">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-4 offset-md-1 mx-3 my-3">

      <!-- Featured image -->
      <div class="view overlay">
        <img src="../img/maps.png" class="img-fluid" alt="Sample image for first version of blog listing">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-7 text-md-left ml-3 mt-3">

      <h4 class="h4 mb-4">Vamos Lá!</h4>
      <p class="font-weight-normal">Digite um endereço: (rua,Cidade,Estado ou País)</p>
      <input type="text" id="txtEndereco" class="form-control"/><br>
      <a href="<?= base_url('Usuario/Resultado')?>" class="btn btn-dark" id="btnBuscarEndereco" value="Consultar endereco">Consultar endereço</a><br><br>
      <p class="font-weight-normal">Nossa busca retornará a longitude, latidute e o País em que se econtra esse endereço.</p> 
    </div>
  </div>
</div>


    

