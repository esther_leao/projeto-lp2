
<!--Navbar-->
<nav class="navbar navbar-dark black lighten-4">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">LPII</a>

  <!-- Collapse button -->
  <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
    aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="white-text"><i
        class="fas fa-bars fa-1x"></i></span></button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent1">

    <!-- Links -->
    <div class="navbar-nav mr-auto">
      <div class="nav-item">
        <a class="nav-link" href="<?= base_url('Usuario/index')?>">Home</a>
        <a class="nav-link" href="<?= base_url('Usuario/Gisgraphy_definicao')?>">Definição</a>
        <a class="nav-link" href="<?= base_url('Usuario/Pratica')?>">Teste você mesmo!</a></div>
        <a class="nav-link" href="<?= base_url('Usuario/Relatorio')?>">Relatório</a></div>
      </div>
    </div>
    <!-- Links -->

  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->