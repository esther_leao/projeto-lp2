<div class="mt-1 ml-5 mr-5 mb-5">
<!-----------------------------------------------------TESTADO----------------------------------------------------------->

    <!--Accordion wrapper-->
<div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
    <div class="card">
        <div class="card-header unique-color-dark lighten-3 z-depth-1" role="tab" id="heading96">
        <h5 class="mb-0 py-1">
            <a class="white-text" data-toggle="collapse" href="#collapse96" aria-expanded="true"
            aria-controls="collapse96">
            O que é Gisgraphy Web Services?
            </a>
        </h5>
        </div>
        <div id="collapse96" class="collapse show" role="tabpanel" aria-labelledby="heading96"
        data-parent="#accordionEx23">
        <div class="card-body">
            <div class="row my-6">
                <div class="col-md-8">
                     <ul>
                        <li>A Gisgraphy é um geocoder(geocodificador) de software livre que pode ser instalado e executado offline em seus servidores.</li>
                        <li>Fornece uma cobertura mundial de serviços web de geocodificação, geolocalização e , até mesmo, rastreamento de veículos.</li>
                        <li>Tem sua política baseada em dados abertos(como o OpenStreetMap) e é possível encontrar seu código fonte no github. </li>
                        <li>O gisgraphy foi projetado para ser escalável,ter alto desempenho e ser usado em outras linguagens além do Java(XML (padrão), Json, php, Python, YAML, ruby, Atom, e Georss). </li>
                        <li>Como resultado, ele é flexível e poderoso o suficiente para ser usado em muitos casos de uso diferentes.</li>
                    </ul>   
                </div>
                <div class="mr-1 col-md-3">
                    <div class="view">
                    <img src="../img/gisgraphy.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header unique-color-dark lighten-3 z-depth-1" role="tab" id="heading97">
        <h5 class="mb-0 py-1">
            <a class="collapsed white-text" data-toggle="collapse" href="#collapse97"
            aria-expanded="false" aria-controls="collapse97">
            Serviços Oferecidos pelo Gisgraphy
            </a>
        </h5>
        </div>
        <div id="collapse97" class="collapse" role="tabpanel" aria-labelledby="heading97"
        data-parent="#accordionEx23">
        <div class="card-body">
            <div class="row my-6">
                <div class="col-md-8">
                    <ul>
                        <li>Geocoding</li>
                            <dd>É o processo de conversão de endereços em coordenadas geográficas, que são usadas para colocar marcadores ou posicionar o mapa.</dd>
                        <li>Reverse geocoding</li>
                            <dd>
                            A geocodificação reversa é o processo de codificação reversa de uma localização de ponto. Ou seja, com esse processo é possível transformar a latitude e a longitude 
                            em um endereço legível ou nome de um local.
                            Isso permite a identificação de endereços, locais e/ou subdivisões de áreas próximas, como bairros, condado, estado ou país.
                            </dd>
                        <li>Find nearby</li>
                            <dd>
                            Por meio desse processo é possivel encontrar locais, cidades e ruas em torno de um ponto de gps para determinado raio.
                            Os resultados podem ser classificados pela distância do ponto dado. 
                            </dd>
                        <li>Search / autocompletion</li>
                            <dd>
                            Torna possível dizer algumas informações(como: coordenadas, estados, população, elevação, nomes alternativos em vários idiomas e alfabeto) de locais, pontos de interesse, cidades, ruas e CEP's.
                            Há também a opção de preenchimento automático do endereço(conforme você digita), viés de localização, verificação ortográfica, para saber se todas as palavras são necessárias para a busca ou não, 
                            e filtragem de tipo de local.
                            </dd>
                        <li>Street / GTS tracking</li>
                        <dd>
                        Com essa ferramenta é possível encontrar informações como: limite de velocidade,número de pistas,se há pedágio ou não, tipo de superfície, para determinada rua ou local.
                        </dd>
                        <li>Address parser</li>
                        <dd>
                        Divide um único endereço em suas partes individuais: número da casa, tipo de rua, nome da rua, unidade, código postal, estado, país , cidade.
                        Isso torna possível gerenciar caixas postais e formato de endereço em mais de 60 países.<br> 
                        (Este software não é opensource e pode ser usado online como um serviço da web ou uma licença pode ser adquirida para um uso offline ilimitado.)
                        </dd>
                        
                    </ul>
                </div>
                <div class="mr-1 col-md-3">
                    <div class="view">
                    <img src="../img/mapa3.png" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>


