<?php
	defined('BASEPATH') OR exit('no direct script access allowed');
    
    class Usuario extends CI_Controller{
      public function index(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('cardPrincipal');
            $this->load->view('common/footer');
      }

      public function Gisgraphy_definicao(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('Gisgraphy_definicao');
            $this->load->view('common/footer');
      }

      public function Pratica(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('Pratica');
            $this->load->view('common/footer');
      }

      public function Resultado(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('Resultado');
            $this->load->view('common/footer');
      }

      public function Relatorio(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            $this->load->view('Relatorio');
            $this->load->view('common/footer');
      }

      
   }
?>